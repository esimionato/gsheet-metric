package qb9;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import com.google.api.services.sheets.v4.Sheets;

import java.util.Collections;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.io.FileInputStream;
import java.util.ArrayList;

public class Quickstart {

    /** Application name. */
    private static final String APPLICATION_NAME =
        "Google Sheets API Java Quickstart";

    /** Directory to store user credentials for this application. */
    private static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home"), ".credentials/sheets.googleapi.com-javaquickstart");
    /** Global instance of the {@link FileDataStoreFactory}. */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY =
        JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart.
     *  If modifying these scopes, delete your previously saved credentials
     *  at ~/.credentials/sheets.googleapis.com-java-quickstart
     */
    private static final List<String> SCOPES =
        Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException
     */
    public static Credential authorize() throws IOException {
        // Load client secrets.
        //InputStream in =
        //    Quickstart.class.getResourceAsStream("/client_secret.json");
        //GoogleClientSecrets clientSecrets =
        //    GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        //// Build flow and trigger user authorization request.
        //GoogleAuthorizationCodeFlow flow =
        //    new GoogleAuthorizationCodeFlow.Builder(
        //            HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
        //    .setDataStoreFactory(DATA_STORE_FACTORY)
        //    .setAccessType("offline")
        //    .build();
        //Credential credential = new AuthorizationCodeInstalledApp(
        //        flow, new LocalServerReceiver()).authorize("user");
        // GoogleCredential //
        Credential credential = GoogleCredential.fromStream(
                qb9.Quickstart.class.getResourceAsStream("/client_secret.json"))
            .createScoped(SheetsScopes.all());
       //     .createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS_READONLY));
            //.createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS));

        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }

    /**
     * Build and return an authorized Sheets API client service.
     * @return an authorized Sheets API client service
     * @throws IOException
     */
    public static Sheets getSheetsService() throws IOException {
        Credential credential = authorize();
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    public static String spreadsheetId = "1GAhfeHqXZXpbjLsxdp1pHS5m0fmKcFxGKaRT8ZFwJnk";
    // public static String spreadsheetId = "1t6FBUH-CNPJmylLNnm84tTgCdMmzJTN4d0TzlKDPDck";//"/1467775790";

    //public static String range = "Class Data!A1:E1";
    public static String range = "09/07/2017!A2:J";
    public static String inrange = "matilda123!A2:J";
    public static Sheets service;


    public static BatchUpdateSpreadsheetResponse addResponse;
    public static List<Request> updateRequests;

    public static ValueRange valueRange;
    public static List<List<Object>> values;


    public static Sheets.Spreadsheets.Values.Update updateRequest;
    public static UpdateValuesResponse updateResponse;

    public static List<List<Object>> getValuesInRange() throws IOException {
        valueRange = service.spreadsheets().values()
            .get(spreadsheetId, range)
            .execute();
        values = valueRange.getValues();
        return values;
    }

    public static void printValues() throws IOException {
        System.out.println(values);
        if (values == null || values.size() == 0) {
            System.out.println("No data found.");
        } else {
            System.out.println("Name, Majer");
            int i=0;
            for (List row : values ) {
                i++; // Print columns A and E, which correspond to indices 0 and 4.
                System.out.printf("> #%03d# %s, %s\n", i, row.get(0), row.get(4));
            }
        }

    }

    public static void addSheet() throws IOException {

        updateRequests = new ArrayList<>();
        Request request = new Request();
        AddSheetRequest addSheet = new AddSheetRequest();
        SheetProperties prop = new SheetProperties();
        prop.setTitle("matilda123");
        addSheet.setProperties(prop);
        updateRequests.add(request.setAddSheet(addSheet));
        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(updateRequests);
        Sheets.Spreadsheets.BatchUpdate batchRequest =
            service.spreadsheets().batchUpdate(spreadsheetId, requestBody);
        BatchUpdateSpreadsheetResponse addResponse = batchRequest.execute();
    }

    public static void setValuesToSheet() throws IOException {
        ValueRange requestBody = new ValueRange();
        requestBody.setValues(values);
        updateRequest = service.spreadsheets().values().update(spreadsheetId, inrange, requestBody);
        String valueInputOption = "1";
        updateRequest.setValueInputOption(valueInputOption);
        updateResponse = updateRequest.execute();
    }

    public static void run() throws IOException {
        service = getSheetsService();
        getValuesInRange();
        printValues();
        addSheet();
        setValuesToSheet();
    }

    public static void main(String[] args) throws IOException {
        run();
    }
}

